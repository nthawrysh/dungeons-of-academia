use image::{
    GenericImage,
    GenericImageView,
    Rgba,
    RgbaImage,
    ImageResult,
};
use std::path::{Path,PathBuf};
use crate::sprite::Sprite;

pub fn get_asset_dir() -> PathBuf {
    std::env::current_exe().unwrap() // target/(debug|release)/dungeons_of_academia
        .parent().unwrap() // target/(debug|release)
        .join(Path::new("../../assets"))
        .canonicalize().unwrap()
}

/// Assumes default texture format of `wgpu::TextureFormat::Rgba8UnormSrgb`.
pub fn draw(frame: &mut [u8], image: &impl GenericImageView<Pixel=Rgba<u8>>) {
    frame
        .chunks_exact_mut(4)
        .zip(image.pixels())
        .for_each(|(mut frame_pix, image_pix)| {
            (&mut frame_pix).copy_from_slice(&image_pix.2.0);
        });
}

pub fn fit_to_height<I: GenericImageView + 'static>(img: &I, width: u32, height: u32)
-> image::ImageBuffer<I::Pixel, Vec<<I::Pixel as image::Pixel>::Subpixel>>
{
    let mut resized = image::imageops::resize(
            img,
            img.width() * height / img.height(),
            height,
            image::imageops::FilterType::Nearest);
    if resized.width() > width {
        let (scaled_width, scaled_height) = resized.dimensions();
        image::imageops::crop(
            &mut resized,
            (scaled_width - width) / 2,
            0,
            width, height
        ).to_image()
    }
    else { resized }
}

pub fn composit(target: &mut RgbaImage, bg: &RgbaImage, sprites: &[&Sprite]) -> ImageResult<()> {
    target.copy_from(bg, 0, 0); // Redraw background using a memcpy
    for sprite in sprites {
        sprite.draw(target);
    }
    Ok(())
}
