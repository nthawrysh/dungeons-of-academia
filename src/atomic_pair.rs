use std::ops::AddAssign;
use std::sync::{
    Arc, RwLock,
    atomic::{AtomicI32, AtomicU32, Ordering}
};

use winit::dpi::PhysicalSize;

pub struct AtomicPairI32 {
    width:  AtomicI32,
    height: AtomicI32,
}

impl From<(i32, i32)> for AtomicPairI32 {
    fn from((width, height): (i32, i32)) -> Self {
        Self {
            width:  AtomicI32::new(width),
            height: AtomicI32::new(height),
        }
    }
}

impl Into<PhysicalSize<i32>> for &AtomicPairI32 {
    fn into(self) -> PhysicalSize<i32> {
        let (width, height) = self.get();
        PhysicalSize::new(width, height)
    }
}

impl AtomicPairI32 {
    pub const fn new(width: i32, height: i32) -> Self {
        Self {
            width:  AtomicI32::new(width),
            height: AtomicI32::new(height),
        }
    }

    pub fn get(&self) -> (i32, i32) {
        (
            self.width.load(Ordering::SeqCst),
            self.height.load(Ordering::SeqCst),
        )
    }

    pub fn set(&self, width: i32, height: i32) {
        self.width.store(width, Ordering::SeqCst);
        self.height.store(height, Ordering::SeqCst);
    }
}

pub struct AtomicPairU32 {
    width:  AtomicU32,
    height: AtomicU32,
}

impl From<(u32, u32)> for AtomicPairU32 {
    fn from((width, height): (u32, u32)) -> Self {
        Self {
            width:  AtomicU32::new(width),
            height: AtomicU32::new(height),
        }
    }
}

impl Into<PhysicalSize<u32>> for &AtomicPairU32 {
    fn into(self) -> PhysicalSize<u32> {
        let (width, height) = self.get();
        PhysicalSize::new(width, height)
    }
}

impl AtomicPairU32 {
    pub const fn new(width: u32, height: u32) -> Self {
        Self {
            width:  AtomicU32::new(width),
            height: AtomicU32::new(height),
        }
    }

    pub fn get(&self) -> (u32, u32) {
        (
            self.width.load(Ordering::SeqCst),
            self.height.load(Ordering::SeqCst),
        )
    }

    pub fn set(&self, width: u32, height: u32) {
        self.width.store(width, Ordering::SeqCst);
        self.height.store(height, Ordering::SeqCst);
    }
}

