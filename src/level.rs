use image::RgbaImage;
use image::error::{ImageResult, ImageError};

use bztree::BzTree;
use crossbeam_epoch::Guard;

use std::error::Error;
use std::io;
use std::path::{Path,PathBuf};
use std::sync::{
    Arc, Mutex, MutexGuard,
    atomic::{AtomicU8, Ordering}
};

use crate::AtomicPairI32;

const INITIAL_POSITION: (i32, i32) = (0, 4);
const INITIAL_DIRECTION: Cardinal = Cardinal::North;

lazy_static::lazy_static! {
    pub static ref LEVEL: Level = Level::new(&crossbeam_epoch::pin());
}

pub fn draw(pixels: &mut pixels::Pixels, guard: &Guard) {
    LEVEL.get_cur_room(guard).draw(
        pixels,
        LEVEL.get_direction())
}

/// Handles errors loading image. Panics on I/O or image errors;
/// returns `None` if the image merely doesn't exist.
pub fn catch<T>(res: ImageResult<T>) -> Option<T> {
    match res {
        Ok(t) => Some(t),

        // Find out if the room simply doesn't exist.
        Err(image::ImageError::IoError(e)) => {
            let Some(inner) = e.get_ref() else { panic!("{e}") };
            match inner.downcast_ref::<NoSuchRoom>() {
                Some(_) => None,
                None => panic!("{e}"),
            }
        },

        Err(e) => panic!("{e}")
    }
}

macro_rules! get_room_path {
    ($pos:expr) => {
        {
            let room_path = Room::get_path($pos);
            if !room_path.exists() {
                return ImageResult::Err(
                    ImageError::IoError(
                        io::Error::new(
                            io::ErrorKind::NotFound,
                            NoSuchRoom {})))
            }
            room_path
        }
    }
}


#[derive(Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum Cardinal {
    North,
    East,
    South,
    West
}

impl Cardinal {
    pub fn rotate_right(self) -> Self {
        match self {
            Self::North => Self::East,
            Self::East  => Self::South,
            Self::South => Self::West,
            Self::West  => Self::North,
        }
    }

    pub fn rotate_left(self) -> Self {
        match self {
            Self::North => Self::West,
            Self::East  => Self::North,
            Self::South => Self::East,
            Self::West  => Self::South,
        }
    }

    pub fn opposite(self) -> Self {
        match self {
            Self::North => Self::South,
            Self::East  => Self::West,
            Self::South => Self::North,
            Self::West  => Self::East,
        }
    }

    pub fn around(self) -> [Self; 4] {
        [
            self,
            self.rotate_right(),
            self.rotate_left(),
            self.opposite()
        ]
    }

    pub fn forward(self) -> (i32, i32) {
        match self {
            Self::North => ( 0,  1),
            Self::East  => ( 1,  0),
            Self::South => ( 0, -1),
            Self::West  => (-1,  0),
        }
    }

    pub fn backward(self) -> (i32, i32) {
        match self {
            Self::North => ( 0, -1),
            Self::East  => (-1,  0),
            Self::South => ( 0,  1),
            Self::West  => ( 1,  0),
        }
    }
}

impl std::fmt::Display for Cardinal {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_str(
            match self {
                Self::North => "north",
                Self::East  => "east",
                Self::South => "south",
                Self::West  => "west",
            })
    }
}

impl From<u8> for Cardinal {
    fn from(src: u8) -> Self {
        match src {
            0 => Self::North,
            1 => Self::East,
            2 => Self::South,
            3 => Self::West,
            _ => panic!("Cannot convert {src} to cardinal direction")
        }
    }
}

impl From<AtomicU8> for Cardinal {
    fn from(src: AtomicU8) -> Self {
        Self::from(src.load(Ordering::SeqCst))
    }
}

impl Into<&'static str> for Cardinal {
    fn into(self) -> &'static str {
        match self {
            Self::North => "north",
            Self::East  => "east",
            Self::South => "south",
            Self::West  => "west",
        }
    }
}

impl std::ops::Add<(i32, i32)> for Cardinal {
    type Output = (i32, i32);

    fn add(self, pos: (i32, i32)) -> (i32, i32) {
        let delta = self.forward();
        (
            pos.0 + delta.0,
            pos.1 + delta.1,
        )
    }
}


#[derive(Debug)]
pub struct NoSuchRoom;

impl std::error::Error for NoSuchRoom {}

impl std::fmt::Display for NoSuchRoom {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "No such room")
    }
}


#[derive(Clone, Default)]
pub struct Edge {
    original_image: image::RgbaImage,
    scaled_image:   image::RgbaImage,
    last_win_size:  (u32, u32),
}

impl Edge {
    pub fn load(room_path: &Path, direction: Cardinal) -> ImageResult<Self> {
        let path = Path::join(
            &room_path,
            direction.to_string() + ".jpg");
        let original_image = image::open(&path)?.into_rgba8();
        let scaled_image = RgbaImage::new(1, 1);

        let mut this = Self {
            original_image,
            scaled_image,
            last_win_size: (0, 0), // Deliberately invalid
        };

        Ok(this)
    }

    pub fn lock_and_load<'a, 'b>(
        place: &'a Mutex<Option<Self>>,
        room_pos: (i32, i32),
        direction: Cardinal
    ) -> ImageResult<MutexGuard<'a, Option<Self>>> {
        let mut guard = place.lock().unwrap();

        if guard.is_none() {
            let room_path = get_room_path!(room_pos);
            *guard = Some(Self::load(&room_path, direction.into())?)
        }
        Ok(guard)
    }

    /// Idempotent; does nothing if the image is already the right size.
    // The ideal way to implement image scaling would be using a GPU shader.
    // Unfortunately `pixels` doesn't support fractinal canvas scaling, so
    // we'll have to make do on the CPU side.
    pub fn resize_image(&mut self) -> bool {
        let (width, height) = crate::WINDOW_SIZE.get();
        if self.last_win_size == (width, height) {
            return false;
        }
        self.last_win_size = (width, height);
        self.scaled_image = crate::images::fit_to_height(
            &self.original_image, width, height);

        return true;
    }

    pub fn ensure_pixbuf_size(&self, pixels: &mut pixels::Pixels) {
        let pixels::wgpu::Extent3d { width, height, .. }
            = pixels.context().texture_extent;
        let image_dims = self.image_dimensions();

        if (width, height) == image_dims { return; }

        pixels.resize_buffer(image_dims.0, image_dims.1);
    }

    pub fn draw(&mut self, pixels: &mut pixels::Pixels) {
        self.resize_image();
        self.ensure_pixbuf_size(pixels);
        crate::images::draw(pixels.get_frame_mut(), &self.scaled_image);
        pixels.render().unwrap();
    }

    pub fn image_width(&self) -> u32 {
        self.scaled_image.width()
    }

    pub fn image_height(&self) -> u32 {
        self.scaled_image.height()
    }

    pub fn image_dimensions(&self) -> (u32, u32) {
        self.scaled_image.dimensions()
    }

    pub fn get_scaled_image(&mut self) -> &mut RgbaImage {
        self.resize_image();
        &mut self.scaled_image
    }
}


#[derive(Clone, Default)]
pub struct Room {
    pub pos:   (i32, i32),
    pub north: Arc<Mutex<Option<Edge>>>,
    pub south: Arc<Mutex<Option<Edge>>>,
    pub east:  Arc<Mutex<Option<Edge>>>,
    pub west:  Arc<Mutex<Option<Edge>>>,
}

impl Room {
    pub fn get_path((x, y): (i32, i32)) -> PathBuf {
        crate::get_asset_dir().join(
            Path::new(&format!("level/{x},{y}")))
    }

    pub fn load_eagerly(x: i32, y: i32) -> ImageResult<Self> {
        let room_path = get_room_path!((x, y));

        Ok(Self {
            pos:   (x, y),
            north: Arc::new(Mutex::new(Some(Edge::load(&room_path, Cardinal::North)?))),
            east:  Arc::new(Mutex::new(Some(Edge::load(&room_path, Cardinal::East)?))),
            south: Arc::new(Mutex::new(Some(Edge::load(&room_path, Cardinal::South)?))),
            west:  Arc::new(Mutex::new(Some(Edge::load(&room_path, Cardinal::West)?))),
        })
    }

    pub fn load_lazily(x: i32, y: i32, directions_to_load: &[Cardinal]) -> ImageResult<Self> {
        let room_path = get_room_path!((x, y));

        let this = Self {
            pos:   (x, y),
            north: Arc::default(),
            east:  Arc::default(),
            south: Arc::default(),
            west:  Arc::default(),
        };
        for dir in directions_to_load {
            this.lock_and_load(*dir)?;
        }
        Ok(this)
    }

    pub fn get_edge(&self, direction: Cardinal) -> &Arc<Mutex<Option<Edge>>> {
        match direction {
            Cardinal::North => &self.north,
            Cardinal::East  => &self.south,
            Cardinal::South => &self.east,
            Cardinal::West  => &self.west,
        }
    }

    /// Loads the edge and returns its lock guard. Note that if the returned
    /// `ImageResult` is `Ok`, then the `Option` inside it is always `Some`.
    pub fn lock_and_load(&self, direction: Cardinal) -> ImageResult<MutexGuard<Option<Edge>>> {
        Edge::lock_and_load(&*self.get_edge(direction), self.pos, direction)
    }

    pub fn draw(&self, pixels: &mut pixels::Pixels, direction: Cardinal) {
        self.lock_and_load(direction).unwrap()
            .as_mut().unwrap()
            .draw(pixels);
    }

    pub fn backward(&self, direction: Cardinal) {
        let delta = direction.backward();
        let new_x = self.pos.0 + delta.0 as i32;
        let new_y = self.pos.1 + delta.1 as i32;
        // The `direction_to_load` argument is correct; the user
        // will be looking at where they just were.
        let Some(next_room) = Self::catch(
            Self::load_lazily(new_x, new_y, &[direction])) else { return };
        LEVEL.set_position(new_x, new_y);
    }

    /// Handles errors loading image. Panics on I/O or image errors;
    /// returns `None` if the image merely doesn't exist.
    pub fn catch(res: ImageResult<Room>) -> Option<Room> {
        match res {
            Ok(new_room) => Some(new_room),

            // Find out if the room simply doesn't exist.
            Err(image::ImageError::IoError(e)) => {
                let Some(inner) = e.get_ref() else { panic!("{e}") };
                match inner.downcast_ref::<NoSuchRoom>() {
                    Some(_) => None,
                    None => panic!("{e}"),
                }
            },

            Err(e) => panic!("{e}")
        }
    }

    pub fn around(&self, facing: Cardinal) -> [(Cardinal, Arc<Mutex<Option<Edge>>>); 4] {
        let right  = facing.rotate_right();
        let left   = facing.rotate_left();
        let behind = facing.opposite();
        [
            (facing, self.get_edge(facing).clone()),
            (right,  self.get_edge(right).clone()),
            (left,   self.get_edge(left).clone()),
            (behind, self.get_edge(behind).clone()),
        ]
    }
}

pub struct Level {
    cur_pos: AtomicPairI32,
    cur_direct: AtomicU8,
    rooms: BzTree<(i32, i32), Room>
}

impl Level {
    pub fn new(guard: &Guard) -> Self {
        let mut me = Self {
            cur_pos: AtomicPairI32::from(INITIAL_POSITION),
            cur_direct: AtomicU8::new(INITIAL_DIRECTION as u8),
            rooms: BzTree::new(),
        };
        me.rooms.upsert(
            INITIAL_POSITION,
            Room::load_lazily(
                INITIAL_POSITION.0,
                INITIAL_POSITION.1,
                &[INITIAL_DIRECTION]).unwrap(),
            guard);
        me
    }

    pub fn get_position(&self) -> (i32, i32) {
        self.cur_pos.get()
    }

    pub fn set_position(&self, new_x: i32, new_y: i32) {
        self.cur_pos.set(new_x, new_y);
    }

    pub fn move_in(&self, direction: Cardinal, guard: &Guard) -> bool {
        let new_pos = direction + self.get_position();

        let Some(next_room) = self.get_room(new_pos, &[self.get_direction()], guard)
            else { return false };
        self.set_position(new_pos.0, new_pos.1);

        true
    }

    pub fn get_direction(&self) -> Cardinal {
        Cardinal::from(self.cur_direct.load(Ordering::SeqCst))
    }

    pub fn set_direction(&self, dir: Cardinal) {
        self.cur_direct.store(dir as u8, Ordering::SeqCst);
    }

    pub fn get_room<'g>(
        &'g self,
        pos: (i32, i32),
        dirs_to_load: &[Cardinal],
        guard: &'g Guard
    ) -> Option<&'g Room> {
        self.rooms.get(&pos, guard).or_else(|| {
            if let Some(room) = Room::catch(
                Room::load_lazily(
                    pos.0, pos.1,
                    dirs_to_load))
            {
                self.rooms.insert(pos, room, guard);
                self.rooms.get(&pos, guard)
            }
            else { None }
        })
    }

    pub fn get_cur_room<'g>(&'g self, guard: &'g Guard) -> &'g Room {
        self.rooms.get(&self.cur_pos.get(), guard).unwrap()
    }

    pub fn get_cur_edge<'g>(&'g self, guard: &'g Guard) -> MutexGuard<'g, Option<Edge>> {
        catch(self.get_cur_room(guard)
            .lock_and_load(self.get_direction())
        ).unwrap()
    }

    pub fn get_adjacent_rooms<'g>(&'g self, start_dir: Cardinal, guard: &'g Guard)
    -> [((i32, i32), Cardinal, Option<&'g Room>); 4] {
        let dirs = self.get_direction().around();
        let cur_pos = self.get_position();
        [
            {
                let adj_pos = dirs[0] + cur_pos;
                (adj_pos, dirs[0], self.get_room(adj_pos, &[], guard))
            },
            {
                let adj_pos = dirs[1] + cur_pos;
                (adj_pos, dirs[1], self.get_room(adj_pos, &[], guard))
            },
            {
                let adj_pos = dirs[2] + cur_pos;
                (adj_pos, dirs[2], self.get_room(adj_pos, &[], guard))
            },
            {
                let adj_pos = dirs[3] + cur_pos;
                (adj_pos, dirs[3], self.get_room(adj_pos, &[], guard))
            },
        ]
    }
}
