use winit::event::{MouseButton, ElementState};

use crate::sprite::Sprite;

use std::f64::consts::PI;

const STARTING_POS: (f64, f64) = (0.0, 0.0);
/// How many pixels per frame our penguin moves.
const WALK_SPEED: f64 = 20.0;
/// From how many pixels away the penguin can snap to its target.
/// Should roughly equal WALK_SPEED.
const SNAP_DISTANCE: f64 = WALK_SPEED + 1.0;

// Vector utilities. I should really use a third-party library for these,
// but they should be fine here for now.

fn round_vec(v: (f64, f64)) -> (u32, u32) {
    (
        v.0.round() as u32,
        v.1.round() as u32,
    )
}

fn vec_length_squared(v: (f64, f64)) -> f64 {
    v.0 * v.0 + v.1 * v.1
}

fn vec_length(v: (f64, f64)) -> f64 {
    f64::sqrt(vec_length_squared(v))
}

/// Scales a vector to length 1.
fn normalize(v: (f64, f64)) -> (f64, f64) {
    let len = vec_length(v);
    (
        v.0 / len,
        v.1 / len,
    )
}

// On to the actual module contents...

enum State {
    Idle,
    Walking {
        target: (f64, f64),
        direction: (f64, f64),
    }
}

pub struct Player {
    sprite: Sprite,
    state: State,
    // I had been just using `self.sprite.pos`, but sprites currently have
    // to be at an integer position, and for movement we need the ability
    // to be at fractional positions.
    pos: (f64, f64),
}

impl Player {
    pub fn new() -> Self {
        Self {
            sprite: Sprite::load(
                "TenderBud".into(),
                round_vec(STARTING_POS),
                "idle".into()
            ).unwrap(),
            state: State::Idle,
            pos: STARTING_POS,
        }
    }

    pub fn update(&mut self) {
        self.sprite.advance();
        if let State::Walking { target, direction } = self.state {
            self.move_toward(target, direction);
        }
    }

    pub fn on_mouse_button(&mut self, pos: (u32, u32), button: MouseButton, state: ElementState) {
        if button != MouseButton::Left || state != ElementState::Pressed { return; }
        let pos = (pos.0 as f64, pos.1 as f64);
        let my_pos = self.get_pos();

        let dir_norm = normalize((
            pos.0 as f64 - my_pos.0,
            pos.1 as f64 - my_pos.1,
        ));
        let direction = (
            dir_norm.0 * WALK_SPEED,
            dir_norm.1 * WALK_SPEED,
        );

        let angle = dir_norm.0.acos(); // Relative to vector (1.0, 0.0)
        let anim =
            if angle < PI / 8.0 {
                "walk_E"
            }
            else if angle < 3.0 / 8.0 * PI {
                if dir_norm.1 < 0.0 { "walk_NE" }
                else { "walk_SE" }
            }
            else if angle < 5.0 / 8.0 * PI {
                if dir_norm.1 < 0.0 { "walk_N" }
                else { "walk_S" }
            }
            else if angle < 7.0 / 8.0 * PI {
                if dir_norm.1 < 0.0 { "walk_NW" }
                else { "walk_SW" }
            }
            else {
                "walk_W"
            };
        self.sprite.set_animation(anim.to_owned());

        self.state = State::Walking {
            target: pos,
            direction,
        };
    }

    pub fn set_pos(&mut self, new_pos: (f64, f64)) {
        self.pos = new_pos;
        self.sprite.pos = round_vec(new_pos);
    }

    pub fn move_toward(&mut self, target: (f64, f64), direction: (f64, f64)) {
        if self.snap(target) {
            self.sprite.set_animation("idle".to_owned());
        }
        else {
            let last_pos = self.pos;
            self.set_pos((
                last_pos.0 + direction.0,
                last_pos.1 + direction.1,
            ));
        }
    }

    pub fn snap(&mut self, target: (f64, f64)) -> bool {
        // This is not a true Euclidean distnace, but a 'rectangular' distance.
        // Not unlike a calculation I used in I-am-not-a-number.stag, my BattleDot.
        let dist = f64::max(
            (target.0 - self.pos.0).abs(),
            (target.1 - self.pos.1).abs());
        if dist <= SNAP_DISTANCE {
            true
        }
        else { false }
    }

    pub fn get_sprite(&self) -> &Sprite {
        &self.sprite
    }

    pub fn get_pos(&self) -> (f64, f64) {
        self.pos
    }
}
