use crossbeam_epoch::Guard;

use crate::level::{Cardinal, Edge, Room};

use std::sync::{Arc, Mutex, mpsc};
use std::thread;

#[derive(PartialEq, Eq)]
struct State {
    pub pos: (i32, i32),
    pub window_size: (u32, u32),
}

impl State {
    pub fn fetch() -> Self {
        Self {
            pos: crate::level::LEVEL.get_position(),
            window_size: crate::WINDOW_SIZE.get(),
        }
    }

    pub fn is_current(&self) -> bool {
        *self == Self::fetch()
    }
}

fn load_room(
    edges: &[(Cardinal, Arc<Mutex<Option<Edge>>>); 4],
    pos: (i32, i32),
    direction: Cardinal,
    state: &State
) -> bool {
    for (direction, edge) in edges {
        Edge::lock_and_load(&edge, pos, *direction)
            .unwrap()
            .as_mut().unwrap()
            .resize_image();
        if !state.is_current() { return false; }
    }
    true
}

fn load_adjacent_rooms(
    start_dir: Cardinal,
    state: &State,
    guard: &Guard,
) -> bool {
    for (pos, dir, room) in crate::level::LEVEL.get_adjacent_rooms(start_dir, guard) {
        let Some(room) = room else { continue };
        if !load_room(&room.around(dir), pos, dir, state) { return false; }
    }
    true
}

/// Send a message on the returned channel to wake.
pub fn run() -> mpsc::Sender<()> {
    let (sender, recv) = mpsc::channel();
    sender.send(()); // Run eagerly!
    thread::spawn(move || {
        let guard = crossbeam_epoch::pin();
        loop {
            match recv.recv() {
                Ok(()) => (),
                Err(_) => return, // Other half disconnected
            }
            while let Ok(()) = recv.try_recv() {} // Slurp any further messages.

            let state = State::fetch();
            let facing = crate::level::LEVEL.get_direction();
            let edges = {
                let room = crate::level::LEVEL.get_cur_room(&guard);
                room.around(facing)
            };

            if !load_room(&edges, state.pos, facing, &state) { continue }
            if !load_adjacent_rooms(facing, &state, &guard) { continue }
        }
    });
    sender
}
