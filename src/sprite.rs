use image::{
    RgbaImage, Pixel,
    ImageResult, ImageError,
    GenericImage, GenericImageView
};
use std::path::{Path, PathBuf};
use std::time::Duration;

struct Animation {
    pub frames: Vec<RgbaImage>,
    pub cur_frame: usize,
}

impl Animation {
    pub fn load(dir: &Path) -> ImageResult<Self> {
        let frames = std::fs::read_dir(dir)
            .map_err(|e| ImageError::IoError(e))? // Convert outer std::io::Result -> image::ImageResult and propagate
            .map(|entry|
                entry
                    .map(|entry|
                        image::open(entry.path())
                            .map(|img| img.to_rgba8()))
                    .map_err(|e| ImageError::IoError(e))) // Convert per-entry error
            .collect::<ImageResult<ImageResult<Vec<RgbaImage>>>>()??;

        Ok(Self {
            frames,
            cur_frame: 0,
        })
    }

    pub fn get_cur_frame(&self) -> &RgbaImage {
        &self.frames[self.cur_frame]
    }

    pub fn advance(&mut self) {
        self.cur_frame += 1;
        if self.cur_frame >= self.frames.len() {
            self.cur_frame = 0;
        }
    }
}

pub struct Sprite {
    dir: PathBuf,
    pub pos: (u32, u32),
    anim_name: String,
    anim: Animation, // Why did I make it like this?
}

impl Sprite {
    pub fn load(anim_name: String, pos: (u32, u32), init_anim_name: String) -> ImageResult<Self> {
        let sprite_dir = Path::join(
            &crate::get_asset_dir(),
            format!("sprites/{}/", anim_name));
        let anim = Animation::load(&Path::join(&sprite_dir, init_anim_name))?;

        Ok(Self {
            dir: sprite_dir,
            pos,
            anim_name,
            anim,
        })
    }

    pub fn dimensions(&self) -> (u32, u32) {
        self.anim.get_cur_frame().dimensions()
    }

    pub fn draw(&self, target: &mut RgbaImage) {
        let (sprite_u, sprite_v) = self.pos;
        let (w, h) = self.dimensions();
        for (u, v, px) in self.anim.get_cur_frame().enumerate_pixels() {
            let Some(tar) = target.get_pixel_mut_checked(u + sprite_u, v + sprite_v)
                else { continue };
            tar.blend(px);
        }
    }

    pub fn advance(&mut self) {
        self.anim.advance()
    }

    pub fn set_animation(&mut self, new_anim_name: String) -> ImageResult<bool> {
        if new_anim_name == self.anim_name { return Ok(false); }
        self.anim_name = new_anim_name;
        self.anim = Animation::load(&Path::join(&self.dir, &self.anim_name))?;
        Ok(true)

    }
}
