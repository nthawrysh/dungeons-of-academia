mod atomic_pair;
mod level;
mod images;
mod worker;
mod sprite;
mod entity;

pub use crate::images::get_asset_dir;

use image::RgbaImage;

use std::path::Path;
use std::sync::MutexGuard;
use std::time::{Duration, Instant};

use winit::{
    dpi::PhysicalSize,
    event::{
        Event, WindowEvent,
        KeyboardInput, VirtualKeyCode, ElementState, ModifiersState,
    },
    event_loop::{ControlFlow, EventLoop},
    window::{WindowBuilder, Window},
};

use pixels::Pixels;

use crate::atomic_pair::*;
use crate::level::{Cardinal, Room};
use crate::sprite::Sprite;
use crate::entity::Player;

/// Window transparency; requires a compositor.
const TRANSPARENT: bool = true;
/// How many frames per second to draw. Inputs will still be handled faster than this.
const FRAME_RATE: u64 = 10;
const FRAME_PERIOD: Duration = Duration::from_millis(1000 / FRAME_RATE);

// We don't hate globals; we just hate action (as in, mutation) at a distance.
// I wish more languages had Common Lisp's dynamically-scoped variables,
// which can be locally overridden on the stack.
static WINDOW_SIZE: AtomicPairU32 = AtomicPairU32::new(2016, 1512);

fn draw(
    pixels: &mut Pixels,
    img_buf: &mut RgbaImage,
    edge: *mut MutexGuard<'static, Option<level::Edge>>,
    sprites: &[&Sprite]
) {
    let edge = unsafe { (*edge).as_mut().unwrap() };
    edge.resize_image();
    edge.ensure_pixbuf_size(pixels);
    let bg = edge.get_scaled_image();
    crate::images::composit(img_buf, &bg, sprites);
    crate::images::draw(pixels.get_frame_mut(), img_buf);
    pixels.render();
}

fn main() {
    let event_loop = EventLoop::new();
    let (init_width, init_height) = WINDOW_SIZE.get();
    let window = WindowBuilder::new()
        .with_title("Dungeons of Academia")
        .with_transparent(TRANSPARENT)
        .with_inner_size(Into::<PhysicalSize<u32>>::into(&WINDOW_SIZE))
        .build(&event_loop).unwrap();

    let mut pixels = {
        let texture = pixels::SurfaceTexture::new(
            init_width, init_height, &window);

        Pixels::new(init_width, init_height, texture).unwrap()
    };
    if TRANSPARENT {
        pixels.set_clear_color(pixels::wgpu::Color::TRANSPARENT);
    }
    let mut cursor_pos: (u32, u32) = (0, 0);
    let mut margin_lr: u32 = 0;

    let bg_wake = crate::worker::run();
    let guard = crossbeam_epoch::pin();

    // These heap-allocation shenanigans are here because of that
    // closure we have to pass into `event_loop.run`. The borrow checker
    // is generally not flexible enough to let me pass mutable state into
    // the closure.
    let edge_guard = Box::into_raw(Box::new(crossbeam_epoch::pin()));
    let mut cur_edge: *mut MutexGuard<'static, Option<level::Edge>>
        = Box::into_raw(
            Box::new(
                level::LEVEL.get_cur_edge(
                    unsafe { &*edge_guard })));

    let mut player = Player::new();
    let mut image_buffer = RgbaImage::new(init_width, init_height);
    let mut last_time = std::time::Instant::now();

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::WaitUntil(last_time + FRAME_PERIOD);
        let cur_time = Instant::now();
        let delta_t = cur_time.duration_since(last_time);
        let mut should_update = delta_t >= FRAME_PERIOD;

        match event {
            Event::RedrawRequested(_) => draw(&mut pixels, &mut image_buffer, cur_edge, &[player.get_sprite()]),

            Event::WindowEvent { event: win_event, window_id, }
            if window_id == window.id() => {
                match win_event {
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,

                    WindowEvent::CursorMoved { position, .. } => {
                        let mut new_cursor_pos: (u32, u32) = position.into();
                        if new_cursor_pos.0 >= margin_lr && new_cursor_pos.0 < margin_lr + image_buffer.width() {
                            new_cursor_pos.0 -= margin_lr;
                            cursor_pos = new_cursor_pos;
                        }
                    }

                    WindowEvent::MouseInput { state, button, .. } => {
                        player.on_mouse_button(cursor_pos, button, state);
                    }

                    WindowEvent::KeyboardInput {
                        input: KeyboardInput {
                            virtual_keycode: Some(keycode),
                            state: ElementState::Pressed, ..
                        }, ..
                    } => {
                        match keycode {
                            VirtualKeyCode::Up => {
                                if level::LEVEL.move_in(level::LEVEL.get_direction(), &guard) {
                                    bg_wake.send(());
                                    unsafe {
                                        *edge_guard = crossbeam_epoch::pin();
                                        *cur_edge = level::LEVEL.get_cur_edge(&*edge_guard);
                                    }
                                    should_update = true;
                                }
                            },
                            VirtualKeyCode::Down => {
                                if level::LEVEL.move_in(level::LEVEL.get_direction().opposite(), &guard) {
                                    bg_wake.send(());
                                    unsafe {
                                        *edge_guard = crossbeam_epoch::pin();
                                        *cur_edge = level::LEVEL.get_cur_edge(&*edge_guard);
                                    }
                                    should_update = true;
                                }
                            },
                            VirtualKeyCode::Right => {
                                let dir = level::LEVEL.get_direction().rotate_right();
                                level::LEVEL.set_direction(dir);
                                unsafe {
                                    *edge_guard = crossbeam_epoch::pin();
                                    *cur_edge = level::LEVEL.get_cur_edge(&*edge_guard);
                                }
                                should_update = true;
                            },
                            VirtualKeyCode::Left => {
                                let dir = level::LEVEL.get_direction().rotate_left();
                                level::LEVEL.set_direction(dir);
                                unsafe {
                                    *edge_guard = crossbeam_epoch::pin();
                                    *cur_edge = level::LEVEL.get_cur_edge(&*edge_guard);
                                }
                                should_update = true;
                            },
                            _ => (),
                        }
                    }

                    WindowEvent::Resized(_) => {
                        let win_size = window.inner_size();
                        let PhysicalSize { width, height } = win_size;

                        WINDOW_SIZE.set(width, height);

                        pixels.resize_buffer(1, 1); // Mitigates a panic when buffer is larger than surface.
                        pixels.resize_surface(width, height);

                        let edge = unsafe { (*cur_edge).as_mut().unwrap() };
                        edge.resize_image(); // TODO: Why not have this return the new dimensions?
                        let (img_width, img_height) = edge.image_dimensions();
                        image_buffer = RgbaImage::new(img_width, img_height);
                        margin_lr = (width - img_width) / 2;

                        bg_wake.send(());

                        should_update = true;
                    },
                    _ => ()
                }
            },
            _ => ()
        }

        if should_update {
            draw(&mut pixels, &mut image_buffer, cur_edge, &[player.get_sprite()]);
            player.update();
            last_time = cur_time;
        }
    });
}
